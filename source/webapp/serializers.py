from drf_extra_fields.fields import Base64ImageField
from rest_framework import serializers

from webapp.models import ArtObject, Category, Author, Event


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ['name']


class AuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Author
        fields = ['first_name', 'last_name', 'biography', 'photo']


class ListArtObjectSerializer(serializers.ModelSerializer):
    category = serializers.StringRelatedField()
    author = serializers.StringRelatedField()

    class Meta:
        model = ArtObject
        fields = ("pk", 'name', 'category', 'material', 'period', 'size', 'photo', 'created_at', 'author')


class DetailArtObjectSerializer(serializers.ModelSerializer):
    author = AuthorSerializer()
    category = serializers.StringRelatedField()

    class Meta:
        model = ArtObject
        fields = ('name', 'category', 'material', 'period', 'size', 'photo', 'description', 'author', 'created_at')


class EventSerializer(serializers.ModelSerializer):
    occurs_at = serializers.DateTimeField(format="%d-%m-%Y %H:%M:%S")

    class Meta:
        model = Event
        fields = ('pk', 'name', 'description', 'photo', 'occurs_at')


class CreateUpdateEventSerializer(serializers.ModelSerializer):
    occurs_at = serializers.DateTimeField(format="%d-%m-%Y %H:%M:%S", input_formats=['%d-%m-%Y %H:%M:%S'])
    photo = Base64ImageField()

    class Meta:
        model = Event
        fields = ('name', 'description', 'photo', 'occurs_at')