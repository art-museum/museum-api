from django.contrib import admin
from django.utils.html import format_html

from webapp.models import Category, Author, ArtObject, Event


class CategoryTabular(admin.TabularInline):
    model = Category


class ArtObjectAdmin(admin.ModelAdmin):
    def image_tag(self, obj):
        return format_html('<img src="{}" height="150" width="150" />'.format(obj.photo.url))

    image_tag.short_description = 'Photo'

    list_display = ['pk', 'name', 'category', 'material', 'size', 'image_tag', 'created_at', 'uploaded_at']
    list_display_links = ['pk', 'name']


class EventAdmin(admin.ModelAdmin):
    def image_tag(self, obj):
        return format_html('<img src="{}" height="150" width="150" />'.format(obj.photo.url))

    image_tag.short_description = 'Photo'
    list_display = ['pk', 'name', 'description', 'occurs_at', 'image_tag']
    list_display_links = ['pk', 'name']


class CategoryAdmin(admin.ModelAdmin):
    fields = ['name', 'parent', 'description']
    list_display = ['pk', 'name', 'parent']
    list_display_links = ['pk', 'name']
    inlines = [CategoryTabular]


admin.site.register(Category, CategoryAdmin)
admin.site.register(Author)
admin.site.register(ArtObject, ArtObjectAdmin)
admin.site.register(Event, EventAdmin)
