from django.shortcuts import render
from rest_framework import viewsets, status
from rest_framework.permissions import DjangoModelPermissionsOrAnonReadOnly
from rest_framework.response import Response

from webapp.models import ArtObject, Event
from webapp.permissions import DjangoModelPermissionsOrIsAuthenticated
from webapp.serializers import DetailArtObjectSerializer, EventSerializer, ListArtObjectSerializer, \
    CreateUpdateEventSerializer
from rest_framework.pagination import PageNumberPagination


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 10
    max_page_size = 10


class ArtObjectViewSet(viewsets.ModelViewSet):
    permission_classes = [DjangoModelPermissionsOrIsAuthenticated]
    serializer_class = DetailArtObjectSerializer
    queryset = ArtObject.objects.all()
    pagination_class = StandardResultsSetPagination

    def get_serializer_class(self):
        if self.action == 'list':
            return ListArtObjectSerializer
        if self.action == 'retrieve':
            return DetailArtObjectSerializer
        else:
            return DetailArtObjectSerializer

    def get_queryset(self):
        return super().get_queryset().order_by('-uploaded_at')


class EventViewSet(viewsets.ModelViewSet):

    serializer_class = EventSerializer
    queryset = Event.objects.all()
    permission_classes = [DjangoModelPermissionsOrIsAuthenticated]

    def get_queryset(self):
        return super().get_queryset().order_by('-pk')

    def get_serializer_class(self):
        if self.action == 'create' or self.action == 'update':
            return CreateUpdateEventSerializer
        else:
            return EventSerializer

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response(status=status.HTTP_200_OK, data={"detail": "ok"})
