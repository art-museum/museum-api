from django.urls import path, include
from rest_framework import routers

from webapp.views import ArtObjectViewSet, EventViewSet

router = routers.DefaultRouter()
router.register(r'artobjects', ArtObjectViewSet)
router.register(r'events', EventViewSet)

app_name = 'webapp'
urlpatterns = [
    path('',include(router.urls))
]