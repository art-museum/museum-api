from rest_framework import permissions
from rest_framework.permissions import SAFE_METHODS


class DjangoModelPermissionsOrIsAuthenticated(permissions.DjangoModelPermissions):
    def has_permission(self, request, view):
        if request.method in SAFE_METHODS:
            return request.user.is_authenticated
        else:
            return super().has_permission(request, view)
