import uuid
from datetime import datetime

from django.core.validators import MinValueValidator
from django.db import models
from django.utils import timezone


class Author(models.Model):
    first_name = models.CharField(verbose_name='Имя', max_length=128)
    last_name = models.CharField(verbose_name='Фамилия', max_length=128)
    middle_name = models.CharField(verbose_name='Отчество', max_length=128, null=True, blank=True)
    biography = models.TextField(verbose_name='Биография', max_length=2048)
    photo = models.ImageField(verbose_name='Фото', blank=True, null=True, upload_to='author_images')
    date_of_birth = models.DateField(verbose_name='Дата рождения', null=True, blank=True)
    date_of_death = models.DateField(verbose_name='Дата смерти', null=True, blank=True)

    def __str__(self):
        return "{} {}".format(self.last_name, self.first_name)


class Category(models.Model):
    name = models.CharField(verbose_name='Название', max_length=128)
    parent = models.ForeignKey('self', verbose_name='Родительская категория', null=True, blank=True,
                               on_delete=models.CASCADE)
    description = models.TextField(verbose_name='Описание', max_length=9016, null=True, blank=True)

    def __str__(self):
        return self.name


class ArtObject(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(verbose_name='Название', max_length=256)
    description = models.TextField(verbose_name='Описание', max_length=2048, null=True, blank=True)
    created_at = models.CharField(verbose_name='Дата создания', max_length=64)
    material = models.CharField(verbose_name='Материал/Техника', max_length=256)
    period = models.CharField(verbose_name='Период', max_length=128)
    size = models.CharField(verbose_name='Размеры', max_length=128)
    photo = models.ImageField(verbose_name='Фото', upload_to='art_images', null=True, blank=True)
    author = models.ForeignKey(Author, verbose_name='Автор', on_delete=models.CASCADE)
    category = models.ForeignKey(Category, verbose_name='Категория', on_delete=models.CASCADE)
    uploaded_at = models.DateTimeField(editable=False, default=timezone.now)

    def __str__(self):
        return self.name


class Event(models.Model):
    name = models.CharField(verbose_name='Название', max_length=256)
    description = models.TextField(verbose_name='Описание', max_length=2048)
    occurs_at = models.DateTimeField(verbose_name='Время проведения')
    photo = models.ImageField(verbose_name='Фото', upload_to='event_images')

    def __str__(self):
        return self.name





